﻿using System.IO;

namespace CameraAPI.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //Loading the configuration file (camera.config)
            CameraUtils.LoadConfiguration();
            //reading the compressed json file
            byte[] t = File.ReadAllBytes("camera/tests/testimage3.json");
            //decompressing the json file
            string jsonData = CameraUtils.InflateString(t);
            //Creating a new render Object for the image rendering
            CameraRender render = new CameraRender(false);
            //parsing the planes from the json file and setting them to the render
            render.Planes = CameraUtils.ParsePlanes(jsonData);
            //parsing the sprites from the json file and setting them to the render
            render.Sprites = CameraUtils.ParseSprites(jsonData);
            //parsing the filters from the json file and setting them to the render
            render.Filters = CameraUtils.ParseFilters(jsonData);
            //rendering the image
            render.Begin();
            //saving the image
            CameraUtils.SaveImage(jsonData, render);
            //disposing the garbage!
            render.Dispose();

            //For testing purposes :D
           /* CameraUtils.LoadConfiguration();
            for(int i = 1; i < 7; i++)
            {
                DateTime now = DateTime.Now;
                Console.WriteLine("Started rendering of Test Image #" + i);
                byte[] t = File.ReadAllBytes("camera/tests/testimage" + i + ".json");
                string jsonData = CameraUtils.InflateString(t);
                CameraRender render = new CameraRender();
                render.Planes = CameraUtils.ParsePlanes(jsonData);
                render.Sprites = CameraUtils.ParseSprites(jsonData);
                render.Filters = CameraUtils.ParseFilters(jsonData);
                render.Begin();
                CameraUtils.SaveImage(jsonData, render);
                render.Dispose();
                DateTime now2 = DateTime.Now;
                int seconds = now2.Subtract(now).Seconds;
                Console.WriteLine("Rendered Test Image #" + i + " in " + seconds + " seconds");
            }
            Console.ReadKey();*/
        }
        
    }
}
