﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace CameraAPI
{
    public class CameraRender
    {
        public List<Plane> Planes
        {
            get;set;
        }

        public List<Sprite> Sprites
        {
            get;set;
        }

        public List<Filter> Filters
        {
            get;set;
        }

        public Bitmap Image
        {
            get;set;
        }

        public Bitmap SmallImage
        {
            get;set;
        }

        private Graphics Graphics
        {
            get;set;
        }

        public bool IsRoomThumbnail
        {
            get;set;
        }

        public CameraRender(bool isRoomThumbnail)
        {
            if (isRoomThumbnail)
                this.Image = new Bitmap(110, 110);
            else
            {
                this.Image = new Bitmap(320, 320);
                this.SmallImage = new Bitmap(100, 100);
            }

            this.IsRoomThumbnail = isRoomThumbnail;
        }

        /// <summary>
        /// Begins with the rendering (of the planes, sprites & the filter :D
        /// </summary>
        public void Begin()
        {
            using (Graphics g = Graphics.FromImage(this.Image))
            {
                /* this.Planes.OrderByDescending(p => p.Z).ToList().ForEach(p => p.Render(g, this.Image));
                 this.Sprites.OrderByDescending(s => s.Z).ToList().ForEach(s => s.Render(g, this.Image));
                 this.Filters.ForEach(f => f.Render(g, this.Image));*/
                List<Renderable> rendables = new List<Renderable>();
                this.Planes.ForEach(p => rendables.Add(p));
                this.Sprites.ForEach(p => rendables.Add(p));
                this.Filters.ForEach(p => rendables.Add(p));
                rendables.OrderByDescending(r => r.Z).ToList().ForEach(r => r.Render(g, this.Image));
            }

            if(!IsRoomThumbnail)
            {
                this.SmallImage = new Bitmap(this.Image, new Size(100, 100));
            }
        }

        public void Dispose()
        {
            this.Image.Dispose();
            this.Planes.Clear();
            this.Sprites.Clear();
            this.Filters.Clear();
        }
    }
}
