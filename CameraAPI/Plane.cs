﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace CameraAPI
{
    public class Plane : Renderable
    {
        public Color Color
        {
            get;set;
        }

        public List<Point> Corners
        {
            get; set;
        } = new List<Point>();

        public double Z
        {
            get; set;
        } = 0.0;

        public List<string> Textures
        {
            get; set;
        } = new List<string>();

        public List<Mask> Masks
        {
            get; set;
        } = new List<Mask>();

        public bool BottomAligned
        {
            get; set;
        } = false;

        public Plane()
        {

        }

        /// <summary>
        /// Draws the Plane to the Image
        /// </summary>
        /// <param name="graphics">graphics channel</param>
        /// <param name="bmp">image</param>
        public void Render(Graphics graphics, Bitmap bmp)
        {
            SolidBrush brush = new SolidBrush(this.Color);
            Polygon p = new Polygon();
            this.AddCorners(p);
            Bitmap newBmp = new Bitmap(bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(newBmp);
            g.FillPolygon(brush, p.ToArray());

            if(this.Textures.Count > 0) this.ApplyTexture(p, this.Textures[0], newBmp);
            this.Masks.ForEach(m => this.AddMask(p, newBmp, m));
            graphics.DrawImage(newBmp, new Point(0, 0));
            p.Dispose();
        }

        /// <summary>
        /// Adds every corner in the right order to draw the polygon
        /// </summary>
        /// <param name="p">Polygon</param>
        private void AddCorners(Polygon p)
        {
            p.AddCorner(this.Corners[3]);
            p.AddCorner(this.Corners[2]);
            p.AddCorner(this.Corners[0]);
            p.AddCorner(this.Corners[1]);
        }

        /// <summary>
        /// TODO!! this method only adds the door to the Polygon
        /// </summary>
        /// <param name="p">Polygon</param>
        /// <param name="img">Image</param>
        /// <param name="m">Mask</param>
        private void AddMask(Polygon p, Bitmap img, Mask m)
        {
            string imgPath = CameraUtils.GetSpritePath(m.Name);
            if (m.Name == "door_64") imgPath = "camera/door_64_2.png";
            if (File.Exists(imgPath))
            {
                Bitmap bmp = (Bitmap)Image.FromFile(imgPath);
                if (m.FlipHorizontal) { bmp.RotateFlip(RotateFlipType.RotateNoneFlipX); }
                if (m.FlipVertical) { bmp.RotateFlip(RotateFlipType.RotateNoneFlipY); }
                if (m.Name == "door_64") m.Position = new Point(this.Corners[1].X + m.Position.X, m.Position.X - m.Position.Y - 35);
                ImageTools.ApplyMask(img, bmp, m.Position);
            }
        }

        /// <summary>
        /// Applies the Texture to the Polygon. Draws it in the right angle... No need of rotation method @sant0ro
        /// </summary>
        /// <param name="polygon">Polygon</param>
        /// <param name="name">Texture Name</param>
        /// <param name="img">Image</param>
        private void ApplyTexture(Polygon polygon, string name, Bitmap img)
        {
            bool verticalflip = false, horizonalflip = false;
            if (name.EndsWith("_flipH"))
            {
                horizonalflip = true;
                name = name.Replace("_flipH", "");
            }
            if (name.EndsWith("_flipV"))
            {
                verticalflip = true;
                name = name.Replace("_flipV", "");
            }
            if(!File.Exists(CameraUtils.Value("camera.folders.roomcontent") +  name + ".png"))
            {
                return;
            }
            Bitmap bmp = (Bitmap)Image.FromFile(CameraUtils.Value("camera.folders.roomcontent") + name + ".png");
            ImageTools.Recolor(bmp, this.Color);
            if (verticalflip) bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
            if (horizonalflip) bmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
            List<Point> upperLine = CameraUtils.Line(polygon.ToArray()[0], polygon.ToArray()[3]);
            List<Point> downLine = CameraUtils.Line(polygon.ToArray()[1], polygon.ToArray()[2]);
            int tX = 0;
            int tY = 0;
            int smallerCount = upperLine.Count > downLine.Count ? downLine.Count : upperLine.Count;
            for(int i = 0; i != smallerCount - 1; i++)
            {
                List<Point> diagonalLine = CameraUtils.Line(upperLine[i], downLine[i]);
                if (tY == bmp.Height) tY = 0;
                tX = 0;
                diagonalLine.ForEach(point =>
                {
                    if (tX == bmp.Width) tX = 0;
                    if(point.X > 0 && point.X < img.Width && point.Y > 0 && point.Y < img.Height && CameraUtils.InPolygon(point, polygon.ToArray()))
                    {
                        img.SetPixel(point.X, point.Y, bmp.GetPixel(tX, tY));
                    }
                    tX++;
                });
                diagonalLine.Clear();
                tY++;
            }
            upperLine.Clear();
            downLine.Clear();
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
		    if(this.Color != null){
			    result.Append("RGB Color: R");
			    result.Append(this.Color.R);
			    result.Append(" G");
			    result.Append(this.Color.G);
			    result.Append(" B");
			    result.Append(this.Color.B);
		    }
                result.Append(" Corner Points:");
            this.Corners.ForEach(p => {
                result.Append(" (X " + p.X + " Y " + p.Y + ")");
            });
		    result.Append(" Z: ");
		    result.Append(this.Z);
		    result.Append(" Textures:");
            this.Textures.ForEach(texture => {
                result.Append(" (" + texture + ")");
            });
		    result.Append(" Masks:");
            this.Masks.ForEach(mask =>
            {
                result.Append(" (Name: ");
                result.Append(mask.Name);
                result.Append(" Flip Vertical ? ");
                result.Append(mask.FlipVertical);
                result.Append(" Flip Horizontal ? ");
                result.Append(mask.FlipHorizontal);
                result.Append(" Location: X ");
                result.Append(mask.Position.X);
                result.Append(" Y ");
                result.Append(mask.Position.Y);
                result.Append(")");
            });
		    result.Append(" Bottom Aligned ? ");
		    result.Append(this.BottomAligned);
            return result.ToString();
        }
    }
}
