﻿using System.Drawing;
using System.Text;

namespace CameraAPI
{
    public class Sprite : Renderable
    {
        public Point Position
        {
            get; set;
        }

        public Color Color
        {
            get;set;
        }
        
        public double Z
        {
            get;set;
        }

        public int Alpha
        {
            get; set;
        } = -1;

        public string Name
        {
            get;set;
        }

        public bool FlipHorizontal
        {
            get; set;
        } = false;

        public bool FlipVertical
        {
            get; set;
        } = false;

        public string BlendMode
        {
            get; set;
        } = "";

        public Sprite()
        {

        }

        /// <summary>
        /// Draws the Sprite to the image
        /// </summary>
        /// <param name="graphics">graphics channel</param>
        /// <param name="image">image</param>
        public void Render(Graphics graphics, Bitmap image)
        {
            string spritePath = CameraUtils.GetSpritePath(this.Name);
            if (spritePath != "")
            {
                Bitmap bmp = (Bitmap)Image.FromFile(spritePath);
                if(this.Color != null && !(this.Color.R == 0 && this.Color.G == 0 && this.Color.B == 0)) ImageTools.Recolor(bmp, this.Color);
                if(this.Alpha != -1) ImageTools.Alpha(bmp, Alpha);
                if (this.FlipVertical) bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                if (this.FlipHorizontal) bmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
                if (this.BlendMode.ToLower().Equals("add"))
                {
                    ImageTools.Blend(image, bmp, this.Position);
                }
                else
                {
                    graphics.DrawImage(bmp, this.Position);
                }
                bmp.Dispose();
            }
            //don't draw the part if it's not found...
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append("Name: " + this.Name);
            result.Append(" Location: X " + this.Position.X + " Y " + this.Position.Y);
            if (this.Color != null)
            {
                result.Append(" RGB Color: R" + this.Color.R);
                result.Append(" G" + this.Color.G + " B" + this.Color.B);
            }
            result.Append(" Z: " + this.Z);
            result.Append(" Alpha: " + this.Alpha);
            result.Append(" HorizontalFlip ? " + this.FlipHorizontal);
            result.Append(" VerticalFlip ? " + this.FlipVertical);
            result.Append(" BlendMode: " + this.BlendMode);
            return result.ToString();
        }
    }
}
