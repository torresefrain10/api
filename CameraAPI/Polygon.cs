﻿using System.Collections.Generic;
using System.Drawing;

namespace CameraAPI
{
    public class Polygon
    {
        private List<Point> Points
        {
            get;set;
        }
        public Polygon()
        {
            this.Points = new List<Point>();
        }

        public void AddCorner(Point p)
        {
            this.Points.Add(p);
        }

        public void AddCorner(int x, int y)
        {
            this.Points.Add(new Point(x, y));
        }
        
        public Point[] ToArray()
        {
            return this.Points.ToArray();
        }

        public void Dispose()
        {
            this.Points.Clear();
        }
    }
}
