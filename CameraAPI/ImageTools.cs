﻿using System.Drawing;

namespace CameraAPI
{
    public class ImageTools
    {
        /// <summary>
        /// Recolors the Image with the given color
        /// </summary>
        /// <param name="img">the image to recolor</param>
        /// <param name="dest">the given color</param>
        public static void Recolor(Bitmap img, Color dest)
        {
            for(int x = 0; x < img.Width; x++)
            {
                for(int y = 0; y < img.Height; y++)
                {
                    Color old = img.GetPixel(x, y);
                    Color newColor = Color.FromArgb(
                        old.A,
                        ImageTools.ColorConvert(old.R, dest.R),
                        ImageTools.ColorConvert(old.G, dest.G),
                        ImageTools.ColorConvert(old.B, dest.B)
                        );
                    img.SetPixel(x, y, newColor);
                }
            }
        }

        /// <summary>
        /// Walks through every Pixel and sets the black ones from the mask to transparent
        /// </summary>
        /// <param name="img">The Plane</param>
        /// <param name="bmp">The Mask</param>
        /// <param name="position">The Position</param>
        public static void ApplyMask(Bitmap img, Bitmap bmp, Point position)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    if (position.X + x > 0 && position.X + x < img.Width && position.Y + y > 0 && position.Y + y < img.Height)
                    {
                        Color c = bmp.GetPixel(x, y);
                        if(c.R == 0 && c.G == 0 && c.B == 0)
                        {
                            img.SetPixel(position.X + x, position.Y + y, Color.Transparent);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Walks through every Pixel, compares the old color with the new color, and if the new color is brighter than the old one -> replace it.
        /// </summary>
        /// <param name="img">The Image</param>
        /// <param name="bmp">The Blending Image</param>
        /// <param name="position">The Position of the blending image</param>
        internal static void Blend(Bitmap img, Bitmap bmp, Point position)
        {
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    if (position.X + x > 0 && position.X + x < img.Width && position.Y + y > 0 && position.Y + y < img.Height)
                    {
                        Color old = img.GetPixel(position.X + x, position.Y + y);
                        Color dest = bmp.GetPixel(x, y);
                        Color newColor = Color.FromArgb(
                            dest.A > old.A ? dest.A : old.A,
                            dest.R > old.R ? dest.R : old.R,
                            dest.G > old.G ? dest.G : old.G,
                            dest.B > old.B ? dest.B : old.B
                            );
                        img.SetPixel(position.X + x, position.Y + y, newColor);
                    }
                }
            }
        }

        /// <summary>
        /// Converts the old color to the new one
        /// </summary>
        /// <param name="oldC">old color</param>
        /// <param name="newC">new color</param>
        /// <returns></returns>
        private static int ColorConvert(int oldC, int newC)
        {
            return (int)(((double)newC / 255) * (double)oldC);
        }

        /// <summary>
        /// Sets an Alpha value to the pixel, without screwing down the old & higher value
        /// </summary>
        /// <param name="img">the image</param>
        /// <param name="alpha">the alpha value</param>
        public static void Alpha(Bitmap img, int alpha)
        {
            for (int x = 0; x < img.Width; x++)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    Color old = img.GetPixel(x, y);
                    //idk why old.A has to be smaller than current A... but it works xD
                    Color newColor = Color.FromArgb(old.A < alpha ? old.A : alpha, old.R, old.G, old.B);
                    img.SetPixel(x, y, newColor);
                }
            }
        }
    }
}
