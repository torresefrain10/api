﻿using System.Drawing;

namespace CameraAPI
{
    public class Mask
    {
        public string Name
        {
            get; set;
        } = "";

        public bool FlipVertical
        {
            get; set;
        } = false;

        public bool FlipHorizontal
        {
            get; set;
        } = false;

        public Point Position
        {
            get; set;
        } = new Point(0, 0);
    }
}
