﻿using System.Drawing;

namespace CameraAPI
{
    interface Renderable
    {
        double Z { get; set; }

        void Render(Graphics graphics, Bitmap bmp);
    }
}
