﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Drawing;
using System.Net;
using System.Drawing.Imaging;
using System.Text;

namespace CameraAPI
{
    public class CameraUtils
    {

        private static Dictionary<string, string> Configuration
        {
            get; set;
        } = new Dictionary<string, string>();

        public static void LoadConfiguration(string path = "camera.config")
        {
            File.ReadAllLines(path).ToList().ForEach(line =>
            {
                if (!line.StartsWith("#") && !string.IsNullOrEmpty(line) && line.Contains("=") && line.Split('=').Length == 2)
                {
                    CameraUtils.Configuration.Add(line.Split('=')[0], line.Split('=')[1]);
                }
            });
        }

        public static string Value(string key)
        {
            if (CameraUtils.Configuration.ContainsKey(key))
            {
                return CameraUtils.Configuration[key];
            }
            return "";
        }
        /// <summary>
        /// Decompresses the Compressed JSON Data using DeflateStream
        /// </summary>
        /// <param name="bytes">Input</param>
        /// <returns>Decompressed string</returns>
        public static string InflateString(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes, 2, bytes.Length - 2))
            using (var inflater = new DeflateStream(stream, CompressionMode.Decompress))
            using (var streamReader = new StreamReader(inflater))
            {
                return streamReader.ReadToEnd();
            }
        }

        /// <summary>
        /// Parses every Plane from the provided JSON Data
        /// </summary>
        /// <param name="jsonData">The provided JSON Data</param>
        /// <returns>List with all the parsed planes</returns>
        public static List<Plane> ParsePlanes(string jsonData)
        {
            List<Plane> result = new List<Plane>();
            JObject obj = JObject.Parse(jsonData);
            JArray arr = JArray.Parse(obj["planes"].ToString());
            arr.Values<JObject>().ToList().ForEach(plane =>
            {
                Plane p = new Plane()
                {
                    BottomAligned = (bool)plane["bottomAligned"],
                    Z = (double)plane["z"]
                };
                if (plane["texCols"] != null)
                {
                    List<string> textures = new List<string>();
                    JArray.Parse(plane["texCols"].ToString()).Values<JObject>().ToList().ForEach(t =>
                    {
                        textures.Add((string)((JArray)t["assetNames"])[0]);
                    });
                    p.Textures = textures;
                }
                List<Point> corners = new List<Point>();
                JArray.Parse(plane["cornerPoints"].ToString()).Values<JObject>().ToList().ForEach(c =>
                {
                    corners.Add(new Point((int)c["x"], (int)c["y"]));
                });
                p.Corners = corners;
                if (plane["color"] != null)
                {
                    int c = (int)plane["color"];
                    p.Color = Color.FromArgb((c >> 16) & 0xff, (c >> 8) & 0xff, (c >> 0) & 0xff);
                }
                if (plane["masks"] != null)
                {
                    List<Mask> masks = new List<Mask>();

                    JArray.Parse(plane["masks"].ToString()).Values<JObject>().ToList().ForEach(m =>
                    {
                        Mask mask = new Mask()
                        {
                            Name = (string)m["name"],
                            FlipVertical = (bool)m["flipV"],
                            FlipHorizontal = (bool)m["flipH"],
                            Position = new Point((int)m["location"]["x"], (int)m["location"]["y"])
                        };
                        masks.Add(mask);
                    });
                    p.Masks = masks;
                }
                result.Add(p);
            });
            arr.Clear();
            obj.RemoveAll();
            return result;
        }

        /// <summary>
        /// Parses all Sprites from the provided JSON data.
        /// </summary>
        /// <param name="jsonData">The provided JSON data</param>
        /// <returns>A list with all the parsed sprites</returns>
        public static List<Sprite> ParseSprites(string jsonData)
        {
            List<Sprite> result = new List<Sprite>();
            JObject obj = JObject.Parse(jsonData);
            JArray arr = JArray.Parse(obj["sprites"].ToString());
            arr.Values<JObject>().ToList().ForEach(sprite =>
            {
                Sprite s = new Sprite();
                if (sprite["alpha"] != null)
                {
                    s.Alpha = (int)sprite["alpha"];
                }
                if (sprite["blendMode"] != null)
                {
                    s.BlendMode = (string)sprite["blendMode"];
                }
                if (sprite["color"] != null)
                {
                    int c = (int)sprite["color"];
                    s.Color = Color.FromArgb((c >> 16) & 0xff, (c >> 8) & 0xff, (c >> 0) & 0xff);
                }
                if (sprite["flipV"] != null)
                {
                    s.FlipVertical = (bool)sprite["flipV"];
                }
                if (sprite["flipH"] != null)
                {
                    s.FlipHorizontal = (bool)sprite["flipH"];
                }
                s.Z = (double)sprite["z"];
                s.Name = (string)sprite["name"];
                s.Position = new Point((int)sprite["x"], (int)sprite["y"]);
                result.Add(s);
            });
            arr.Clear();
            obj.RemoveAll();
            return result;
        }

        /// <summary>
        /// Parsing the filters from the JSON Data
        /// </summary>
        /// <param name="jsonData">JSON Data</param>
        /// <returns>List with every Filter</returns>
        public static List<Filter> ParseFilters(string jsonData)
        {
            List<Filter> result = new List<Filter>();
            JObject obj = JObject.Parse(jsonData);
            JArray arr = JArray.Parse(obj["filters"].ToString());
            arr.Values<JObject>().ToList().ForEach(filter =>
            {
                Filter f = new Filter()
                {
                    Alpha = (int)filter["alpha"],
                    Name = (string)filter["name"]
                };
                result.Add(f);
            });
            arr.Clear();
            obj.RemoveAll();
            return result;
        }
        /// <summary>
        /// Calculates all the Positions in the Line, using the Bresenham Algorythm ( https://de.wikipedia.org/wiki/Bresenham-Algorithmus )
        /// </summary>
        /// <param name="start">Start Point</param>
        /// <param name="end">End Point</param>
        /// <returns>List with all Positions</returns>
        public static List<Point> Line(Point start, Point end)
        {
            List<Point> points = new List<Point>();
            int x0 = start.X, y0 = start.Y, x1 = end.X, y1 = end.Y;
            int dx = Math.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = -Math.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = dx + dy, e2;
            while (true)
            {
                points.Add(new Point(x0, y0));
                if (x0 == x1 && y0 == y1) break;
                e2 = 2 * err;
                if(e2 > dy) { err += dy; x0 += sx; }
                if(e2 < dx) { err += dx; y0 += sy; }
            }
            return points;
        }

        /// <summary>
        /// Checks if the sprite exists.. If it can't be found in any folder (sprite folder and clothes folder)
        /// it returns an empty string. If the String begins with https:// it tries to download the image from the internet
        /// </summary>
        /// <param name="name">Sprite Name</param>
        /// <returns>Path</returns>
        public static string GetSpritePath(string name)
        {
            if (File.Exists(CameraUtils.Value("camera.folders.roomcontent") + name + ".png"))
            {
                return CameraUtils.Value("camera.folders.roomcontent") + name + ".png";
            }
            if (File.Exists(CameraUtils.Value("camera.folders.sprites") + name + ".png"))
            {
                return CameraUtils.Value("camera.folders.sprites") + name + ".png";
            }
            if (File.Exists(CameraUtils.Value("camera.folders.clothes") + name + ".png"))
            {
                return CameraUtils.Value("camera.folders.clothes") + name + ".png";
            }

            if (name.StartsWith("https://") && File.Exists(CameraUtils.Value("camera.folders.web") + CameraUtils.Filter(name)))
            {
                return CameraUtils.Value("camera.folders.web") + CameraUtils.Filter(name);
            }else if (name.StartsWith("https://") && (name.EndsWith(".png") || name.EndsWith(".jpg") || name.EndsWith(".gif")))// I don't want to have an executable on my server!
            {
                if (!Directory.Exists(CameraUtils.Value("camera.folders.web"))) Directory.CreateDirectory(CameraUtils.Value("camera.folders.web"));
                WebClient wc = new WebClient();
                wc.DownloadFile(name, CameraUtils.Value("camera.folders.web") + CameraUtils.Filter(name));
                return CameraUtils.Value("camera.folders.web") + CameraUtils.Filter(name);
            }
            return "";
        }

        /// <summary>
        /// Filters the url to a saveable path
        /// </summary>
        /// <param name="url">URL</param>
        /// <returns>Filtered URL</returns>
        public static string Filter(string url)
        {
            return url.Replace(":", "").Replace("/", "-").Replace("%","");
        }

        /// <summary>
        /// Checks if the given Position is in the Polygon thanks to some Stackoverflow guys :D
        /// </summary>
        /// <param name="v">given Position</param>
        /// <param name="p">Polygon Corners</param>
        /// <returns>Is the Position in Polygon or not</returns>
        public static bool InPolygon(Point v, Point[] p)
        {
            int j = p.Length - 1;
            bool c = false;
            for (int i = 0; i < p.Length; j = i++) c ^= p[i].Y > v.Y ^ p[j].Y > v.Y && v.X < (p[j].X - p[i].X) * (v.Y - p[i].Y) / (p[j].Y - p[i].Y) + p[i].X;
            return c;
        }

        /// <summary>
        /// Creates a pseudo-filename using the amount of sprites & planes, roomId, date & time
        /// </summary>
        /// <param name="jsonData">jsonData</param>
        /// <param name="render">render</param>
        /// <returns>Filename</returns>
        public static string CreateFilename(string jsonData, CameraRender render)
        {
            JObject obj = JObject.Parse(jsonData);
            if (render.IsRoomThumbnail)
                return (string)obj["roomid"];

            StringBuilder builder = new StringBuilder();
            builder.Append(obj["roomid"]);
            builder.Append("-");
            builder.Append(obj["timestamp"]);
            obj.RemoveAll();
            return builder.ToString().Replace(":", "").Replace(".", "").Replace("/", "");
        }

        /// <summary>
        /// Saves the Image to the given Path in the configuration (or uploads it to a ftp account)
        /// </summary>
        /// <param name="jsonData">JSON Data</param>
        /// <param name="render">Camera Render</param>
        public static string SaveImage(string jsonData, CameraRender render)
        {
            string filename = CameraUtils.CreateFilename(jsonData, render);
            if (CameraUtils.Value("camera.storage.method").ToLower().Equals("ftp"))
            {
                using (WebClient client = new WebClient())
                using (var ms = new MemoryStream())
                {
                    render.Image.Save(ms, ImageFormat.Png);
                    client.Credentials = new NetworkCredential(CameraUtils.Value("camera.storage.ftp.username"), CameraUtils.Value("camera.storage.ftp.password"));
                    if(render.IsRoomThumbnail)
                        client.UploadData("ftp://" + CameraUtils.Value("camera.storage.ftp.hostname") + "/" + CameraUtils.Value("camera.storage.ftp.folder") + "navigator-thumbnail/hhus/" + filename + ".png", ms.ToArray());
                    else
                    {
                        client.UploadData("ftp://" + CameraUtils.Value("camera.storage.ftp.hostname") + "/" + CameraUtils.Value("camera.storage.ftp.folder") + filename + ".png", ms.ToArray());
                        using (var anotherOne = new MemoryStream())
                        {
                            render.SmallImage.Save(anotherOne, ImageFormat.Png);
                            client.UploadData("ftp://" + CameraUtils.Value("camera.storage.ftp.hostname") + "/" + CameraUtils.Value("camera.storage.ftp.folder") + filename + "_small.png", anotherOne.ToArray());
                        }
                    }
                }
            }
            else if (CameraUtils.Value("camera.storage.method").ToLower().Equals("internal"))
            {
                if (!Directory.Exists(CameraUtils.Value("camera.storage.internal.folder") + "navigator-thumbnail/hhus/"))
                {
                    Directory.CreateDirectory(CameraUtils.Value("camera.storage.internal.folder") + "navigator-thumbnail/hhus/");
                }
                if (render.IsRoomThumbnail)
                    render.Image.Save(CameraUtils.Value("camera.storage.internal.folder") + "navigator-thumbnail/hhus/" + filename + ".png", ImageFormat.Png);
                else
                {
                    render.Image.Save(CameraUtils.Value("camera.storage.internal.folder") + filename + ".png", ImageFormat.Png);
                    render.SmallImage.Save(CameraUtils.Value("camera.storage.internal.folder") + filename + "_small.png", ImageFormat.Png);
                }
            }
            return filename;
        }
    }
}
