﻿using System.Drawing.Imaging;

namespace CameraAPI
{
    public class MatrixFilters
    {
        /// <summary>
        /// Returns the Matrix Filter.
        /// </summary>
        /// <param name="name">name of the filter</param>
        /// <returns>matrix filter</returns>
        public static ColorMatrix GetFilter(string name)
        {
            switch (name.ToLower())
            {
                case "dark_sepia":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.4f, 0.4f, 0.1f, 0, 110 },
                        new float[]{ 0.3f, 0.4f, 0.1f, 0, 30 },
                        new float[]{0.3f, 0.2f, 0.1f, 0, 0 },
                        new float[]{0, 0, 0, 1, 0 },
                        new float[]{0,0,0,0,0}
                    });
                case "increase_saturation":
                    return new ColorMatrix(new float[][] {
                        new float[] { 2, -0.5f, -0.5f, 0, 0 },
                        new float[] {-0.5f, 2, -0.5f, 0, 0 },
                        new float[] {-0.5f, -0.5f, 2, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0}
                    });
                case "increase_contrast":
                    return new ColorMatrix(new float[][] {
                        new float[] { 1.5f, 0, 0, 0, -50 },
                        new float[] {0, 1.5f, 0, 0, -50 },
                        new float[] {0, 0, 1.5f, 0, -50 },
                        new float[] {0, 0, 0, 1.5f, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "color_1":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.393f, 0.769f, 0.189f, 0, 0 },
                        new float[] {0.349f, 0.686f, 0.168f, 0, 0 },
                        new float[] {0.272f, 0.534f, 0.131f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "hue_bright_sat":
                    return new ColorMatrix(new float[][] {
                        new float[] { 1, 0.6f, 0.2f, 0, -50 },
                        new float[] {0.2f, 1, 0.6f, 0, -50 },
                        new float[] {0.6f, 0.2f, 1, 0, -50 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "color_2":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.333f, 0.333f, 0.333f, 0, 0 },
                        new float[] {0.333f, 0.333f, 0.333f, 0, 0 },
                        new float[] {0.333f, 0.333f, 0.333f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "night_vision":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0, 0, 0, 0, 0 },
                        new float[] {0, 1.1f, 0, 0, -50 },
                        new float[] {0, 0, 0, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "decr_conrast":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.5f, 0, 0, 0, 50 },
                        new float[] {0, 0.5f, 0, 0, 50 },
                        new float[] {0, 0, 0.5f, 0, 50 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "green_2":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.5f, 0.5f, 0.5f, 0, 0 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, 90 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "color_3":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.609f, 0.609f, 0.082f, 0, 0 },
                        new float[] {0.309f, 0.609f, 0.082f, 0, 0 },
                        new float[] {0.309f, 0.609f, 0.082f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "color_4":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.8f, -0.8f, 1, 0, 70 },
                        new float[] {0.8f, -0.8f, 1, 0, 70 },
                        new float[] {0.8f, -0.8f, 1, 0, 70 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "hypersaturated":
                    return new ColorMatrix(new float[][] {
                        new float[] { 2, -1, 0, 0, 0 },
                        new float[] {-1, 2, 0, 0, 0 },
                        new float[] {0, -1, 2, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "yellow":
                    return new ColorMatrix(new float[][] {
                        new float[] { 1, 0, 0, 0, 0 },
                        new float[] {0, 1, 0, 0, 0 },
                        new float[] {0, 0, 0, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "x_ray":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0, 1.2f, 0, 0, -100 },
                        new float[] {0, 2, 0, 0, -120 },
                        new float[] {0, 2, 0, 0, -120 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "decrease_saturation":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.7f, 0.2f, 0.2f, 0, 0 },
                        new float[] {0.2f, 0.7f, 0.2f, 0, 0 },
                        new float[] {0.2f, 0.2f, 0.7f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "color_5":
                    return new ColorMatrix(new float[][] {
                        new float[] { 3.309f, 0.609f, 1.082f, 0.2f, 0 },
                        new float[] {0.309f, 0.609f, 0.082f, 0, 0 },
                        new float[] {1.309f, 0.609f, 0.082f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "black_white_negative":
                    return new ColorMatrix(new float[][] {
                        new float[] { -0.5f, -0.5f, -0.5f, 0, 0xFF },
                        new float[] {-0.5f, -0.5f, -0.5f, 0, 0xFF },
                        new float[] {-0.5f, -0.5f, -0.5f, 0, 0xFF },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "blue":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.5f, 0.5f, 0.5f, 0, -255 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, -170 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, 0 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "red":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.5f, 0.5f, 0.5f, 0, 0 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, -170 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, -170 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                case "green":
                    return new ColorMatrix(new float[][] {
                        new float[] { 0.5f, 0.5f, 0.5f, 0, -170 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, 0 },
                        new float[] {0.5f, 0.5f, 0.5f, 0, -170 },
                        new float[] {0, 0, 0, 1, 0 },
                        new float[] {0, 0, 0, 0, 0 }
                    });
                default:
                    return new ColorMatrix();
            }
        }
    }
}
