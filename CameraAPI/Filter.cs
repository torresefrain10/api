﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace CameraAPI
{
    public class Filter : Renderable
    {
        public int Alpha
        {
            get; set;
        } = -1;

        public string Name
        {
            get; set;
        } = "";

        public double Z
        {
            get; set;
        } = double.MinValue;

        public void Render(Graphics graphics, Bitmap bmp)
        {
            if (!this.Name.Equals("texture_overlay") && File.Exists(CameraUtils.Value("camera.folders.filters") + this.Name + ".png"))
            {
                Bitmap filter = (Bitmap)Image.FromFile(CameraUtils.Value("camera.folders.filters") + this.Name + ".png");
                Bitmap newImage = new Bitmap(filter.Width, filter.Height);
                using (Graphics grphs = Graphics.FromImage(newImage))
                {
                    grphs.DrawImage(filter, 0, 0);
                }
                ImageTools.Alpha(newImage, this.Alpha);
                graphics.DrawImage(newImage, new Point(0, 0));
                filter.Dispose();
                newImage.Dispose();
            }
            else if(MatrixFilters.GetFilter(this.Name) != null)
            {
                ImageAttributes attr = new ImageAttributes();
                ColorMatrix matrix = MatrixFilters.GetFilter(this.Name);
                attr.SetColorMatrix(matrix, ColorMatrixFlag.Default,ColorAdjustType.Bitmap);
                Bitmap copy = new Bitmap(bmp, bmp.Width, bmp.Height);
                ImageTools.Alpha(copy, this.Alpha);
                graphics.DrawImage(copy, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel, attr);
                copy.Dispose();
                attr.Dispose();
            }
        }
    }
}
